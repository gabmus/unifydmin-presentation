Uno dei problemi più comuni dell'amministrazione di sistema è la ripetitività delle operazioni che l'amministratore deve effettuare.

Alcune operazioni comuni sono il monitoraggio del carico e dei servizi di un sistema e l'esecuzione di operazioni come aggiornamenti, backup e ripristino a versioni precedenti.

La natura di questo problema non è nuova, ed esistono già software che offrono soluzioni che permettono agli amministratori di automatizzare gran parte dei loro compiti.

Due di questi software sono Ansible e Cockpit, che sono stati delle fonti di ispirazione durante la realizzazione di Unifydmin.

Unifydmin cerca infatti di combinare i lati positivi di questi due strumenti.

Ansible è uno strumento da linea di comando che permette l'automazione di operazioni ripetitive su uno o più sistemi. Permette di raccogliere e catalogare diversi sistemi in file di testo chiamati inventari. Tramite l'utilizzo di vari moduli è possibile creare dei file chiamati playbook, comparabili a script, per eseguire una serie di operazioni su di un sistema o un gruppo di sistemi definito in un inventario. Ansible va installato solamente sul computer dell'amministratore, e comunica con i server utilizzando semplicemente ssh e python per eseguire le operazioni.

Cockpit è un pannello di amministrazione adoperabile via browser. Offre un'interfaccia grafica ch espone all'utente le operazioni di monitoraggio e amministrazione più comuni, come la visualizzazione di log, gestione di macchine virtuali basate su libvirt, gestione di account e servizi systemd e aggiornamento del sistema. Cockpit deve essere installato come pacchetto sul server da amministrare.

Unifydmin, similmente a Cockpit, offre un'interfaccia grafica per l'amministrazione. Mostra dei parametri di monitoraggio come il carico di sistema, i servizi di systemd installati, le porte di rete aperte e i processi di docker e permette di eseguire azioni comuni come aggiornamenti e riavvii di sistema, anche su più sistemi contemporaneamente.

Analogamente ad Ansible, anche Unifydmin va installato solamente sul computer dell'amministratore, utilizzando ssh per comunicare con i vari sistemi remoti.

Inoltre l'interfaccia di Unifydmin è responsiva, in modo tale da potersi adattare anche a formati di schermo più piccoli, come smartphone.

Unifydmin è stato realizzato con il linguaggio Python 3 e il toolkit GTK 3, con l'aggiunta della libreria LibHandy realizzata da Purism per gestire la responsività dell'interfaccia.

Le connessioni SSH sono gesite tramite la libreria Fabric, che offre un'interfaccia di livello più alto per la connessione e l'esecuzione di comandi.

L'applicazione è distribuita come Flatpak, per forire una maggiore compatibilità.

La logica di Unifydmin si basa su 3 astrazioni fondamentali: System, Action e Watcher Action.

Un System rappresenta una macchina remota, con una connessione SSH per eseguire comandi.

Un'Action è un azione eseguibile su uno o più System, come ad esempio l'aggiornamento.

Una Watcher Action invece è un'azione che viene eseguita ripetutamente su un System, e generalmente è associata a operazioni di monitoraggio.

Grazie all'utilizzo della libreria LibHandy, Unifydmin è compatibile con smartphone GNU/Linux in prossima uscita come il Librem 5 di Purism e il Pinephone di Pine64.

Utilizzando Flatpak per la distribuzione, di Unifydmin è automaticamente compatibile con più di 21 distribuzioni Linux, incluso Chrome OS.

Unifydmin è stato un progetto molto interessante da realizzare, e spero che possa essere uno strumento utile per molti amministratori.

Le possibilità per sviluppi futuri sono molteplici, data soprattutto la natura modulare di Unifydmin. Alcuni spunti possono essere l'espansione delle funzionalità aggiungendo altre Action e Watcher Action, l'implementazione di un vero e proprio motore di plugin, o il miglioramenteo dell'infrastruttura multi thread per rendere la raccolta di dati più efficiente.
